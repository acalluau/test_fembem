#include "main.h"

/*! \brief Print help
  \return 0 for success
*/
int printHelp() {

  printf("\ntest_FEMBEM usage:\n-----------------\n\n"
         "     --bem/--fem/--fembem: Numerical model (bem is dense, fem is sparse). Default: bem.\n"
         "     -nbpts: Number of unknowns of the global linear system. Default: 16000 (can be in floating point notation, like 1.2e4).\n"
         "     -nbrhs: Number of right-hand sides of the global linear system. Default: 1.\n"
         "     -s/-d/-c/-z: Scalar type of the computation (real computation uses 1/R kernel in BEM, complex computation uses oscillatory exp(ikr)/r kernel). Default: z.\n"
         "     -v: Verbose mode, to display values of data (extremely verbose). Default is OFF.\n"
         "     -sparserhs: Sparsity of the right hand sides (1 non-zero value every 'n' values). Default is nbpts/80.log10(nbpts).\n"
         "     --new-rhs/--no-new-rhs: Use (or not) new way to compute RHS (old way was producing low rank RHS). Default is ON.\n"
         "     -lambda: Wavelength (for oscillatory kernels). Mutually exclusive with -ptsperlambda. Default lambda is set with 10 points per wavelength.\n"
         "     -ptsperlambda: Points per wavelength on the main cylinder (for oscillatory kernels). Default: 10.\n"
         "     --write-mesh: Write a VTK file 'mesh.vtu' of the mesh. Default is OFF.\n"
         "     --write-mesh-unv: Write a UNV file 'mesh.unv' of the mesh. Default is OFF.\n"
         "     --compute_norm: Compute and display matrix norms during Hmat tests. Default: OFF.\n"
         "\n"
         "     BEM tests use a surfacic cylinder with an optionnal cylinder detail, defined with:\n"
         "     -radius: Radius of the cylinder. Default is 2.\n"
         "     -height: Height of the cylinder. Default is 4.\n"
         "     -with_detail: add a finely meshed cylionder detail. Default is NO.\n"
         "     -radius_detail: Radius of the cylinder detail. Default is 0.02.\n"
         "     -height: Height of the cylinder detail. Default is 0.5.\n"
         "     -ptsperlambda_detail: Points per wavelength on the detail. Default is '10 times finer than on the main cylinder'.\n"
         "\n"
         "     FEM and FEM/BEM tests use a volumic cylinder, defined with:\n"
         "     -radius: Radius of the cylinder. Default is 2.\n"
         "     -height: Height of the cylinder. Default is 4.\n"
         "     For FEM/BEM, the BEM is done on the outer surface of the cylinder (without the upper and lower disks).\n"
         "\n"
         "     Choice of the matrix symmetry (default is --sym):\n"
         "     --sym: Symmetric matrices and solvers.\n"
         "     --nosym: Non-Symmetric matrices and solvers (HMAT only).\n"
         "     --symcont: Symmetric content in a non symmetric matrix and non symmetric solvers.\n"
         "     --posdef: Symmetric positive definite matrices (requires --sym or --symcont).\n"
         "\n"
         "     Choice of the tested algorithm (no default):\n"
       #ifdef HAVE_HMAT
         "     -gemvhmat: gemv HMAT.\n"
         "     -solvehmat: solve HMAT.\n"
         "     -inversehmat: inverse HMAT.\n"
       #else
         "     HMAT solver support not available.\n"
       #endif
       #ifdef HAVE_CHAMELEON
         "     -gemvchameleon: gemv CHAMELEON.\n"
         "     -solvechameleon: solve CHAMELEON.\n"
       #else
         "     CHAMELEON solver support not available.\n"
       #endif
       #ifdef CHAMELEON_USE_HMAT
         "     -gemvhchameleon: gemv H-CHAMELEON.\n"
         "     -solvehchameleon: solve H-CHAMELEON.\n"
       #else
         "     H-CHAMELEON solver support not available.\n"
       #endif
       #ifdef HAVE_HLIBPRO
         "     -gemvhlibpro: gemv HLIBPRO.\n"
         "     -solvehlibpro: solve HLIBPRO.\n"
       #else
         "     HLIBPRO solver support not available.\n"
       #endif
         "\n"
       #ifdef HAVE_HMAT
         "     Options for HMAT tests\n"
         "     --use_simple_assembly: Using simple assembly routine for HMAT. Default: NO.\n"
         "     --hmat_divider: Value of the Hmat clustering divider (number of children per box). Default: 2.\n"
         "     --use_hmat_shuffle: Using Hmat shuffle clustering. Default: NO.\n"
         "     --hmat_divider_min: Value of the Hmat shuffle clustering minimum divider (minimum number of children per box). Default: 2.\n"
         "     --hmat_divider_max: Value of the Hmat shuffle clustering maximum divider (maximum number of children per box). Default: 4.\n"
         "     --hodlr: Using HODLR. Default: NO.\n"
         "     --hmat-eps-assemb: Compression epsilon. Default: 1e-4.\n"
         "     --hmat-eps-recompr: Recompression epsilon. Default: 1e-4.\n"
	 "     --hmat-leaf-size: Maximum leaf size. Default: -1 (set by hmat itself).\n"
         "     Compression algorithm: default is ACA+, it can be changed with:\n"
         "     --hmat-svd: Using SVD compression.\n"
         "     --hmat-aca-partial: Using ACA with partial pivoting.\n"
         "     --hmat-aca-full: Using ACA with full pivoting.\n"
         "     Solver: default is LDLt, it can be changed with:\n"
         "     --hmat-lu: Force LU decomposition.\n"
         "     --hmat-llt: Force LLt decomposition (for symmetric matrices).\n"
         "     --hmat-ldlt: Force LDLt decomposition (for symmetric matrices).\n"
         "     --hmat-coarsening: Coarsening of the HMatrix.\n"
         "     --hmat-recompress: Recompress the HMatrix.\n"
         "     --hmat_residual: Compute the residual norm.\n"
         "     --hmat_refine: Use iterative refinement.\n"
         "     --hmat-val-null: Validate the detection of null rows and columns.\n"
         "     --hmat-val: Validate the rk-matrices after compression.\n"
         "     --hmat-val-threshold: Error threshold for the compression validation.\n"
         "     --hmat-dump-trace: Dump trace at the end of the algorithms (depends on the runtime).\n"
         "     --hmat-val-rerun: For blocks above error threshold, re-run the compression algorithm.\n"
         "     --hmat-val-dump: For blocks above error threshold, dump the faulty block to disk.\n"
         "     Runtime: default (and only choice with hmat-oss) is sequential. With hmat, it can be changed with:\n"
         "     --hmat-toyrt: Use the ToyRT runtime.\n"
         "     --hmat-starpu: Use the StarPU runtime.\n"
         "     --hmat-seq: Use the sequential runtime.\n"
         "\n"
       #endif
       #ifdef HAVE_CHAMELEON
         "     Options for CHAMELEON and H-CHAMELEON tests\n"
         "     --chameleon-ngpus: Number of GPUs\n"
         "     --chameleon-ncpus: Number of CPUs\n"
         "     --chameleon-tile: Tile size\n"
         "\n"
       #endif
       #ifdef HAVE_HLIBPRO
         "     Options for HLIBPRO tests\n"
         "     --hlibpro-eps-assemb: Compression epsilon. Default: 1e-4.\n"
         "     --hlibpro-eps-recompr: Recompression epsilon. Default: 1e-4.\n"
         "     Compression algorithm: default is ACA+, it can be changed with:\n"
         "     --hlibpro-svd: Using SVD compression.\n"
         "     --hlibpro-aca-partial: Using ACA with partial pivoting.\n"
         "     --hlibpro-aca-full: Using ACA with full pivoting.\n"
         "     Solver: default is LDLt, it can be changed with:\n"
         "     --hlibpro-lu: Force LU decomposition.\n"
         "     --hlibpro-llt: Force LLt decomposition (for symmetric matrices).\n"
         "     --hlibpro-ldlt: Force LDLt decomposition (for symmetric matrices).\n"
         "     --hlibpro-leaf-size: Maximum leaf size.\n"
         "     --hlibpro-ncpus: Number of CPUs (default is OMP_NUM_THREADS if it is set, 12 otherwise)\n"
         "\n"
       #endif
         "     -h/--help: Display this help\n\n");

  return 0;
}
