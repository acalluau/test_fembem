#include "main.h"

/*! \brief Realise le produit matrice-vecteur classique, parallele, pour les interactions BEM

  \param ffar vaut 1 si on prend que le lointain, 0 si on prend que le proche, 2 si on prend tout
  \param sol the output vector. Must be allocated and filled with zeros
  \param myComm an MPI Communicator
  \return 0 for success
*/
int produitClassiqueBEM(int ffar, void *sol, MPI_Comm myComm) {
  int jg, ierr, nbProcs, myProc ;

  if (nbPtsBEM==0) return 0;

  enter_context("produitClassiqueBEM");

  Mpf_printf(myComm, "BEM: ");

  ierr = MPI_Comm_rank(myComm, &myProc) ; CHKERRQ(ierr) ;
  ierr = MPI_Comm_size(myComm, &nbProcs) ; CHKERRQ(ierr) ;

  /* Global counter jg = only for progress bar */
  jg=0;
  ierr=Mpf_progressBar(myComm, 0, nbPtsBEM+1) ;
  /* Loop on the columns of the matrix that correspond to non-zero rows of the RHS */
#pragma omp parallel for
  for (int j=0 ; j<nbPtsBEM ; j+=sparseRHS) {
    int i, k, ierr;
    double coord_i[4], coord_j[3];
    Z_type Aij ;
    Z_type *z_sol=sol, *z_rhs=rhs ;
    D_type *d_sol=sol, *d_rhs=rhs ;

    ierr=computeCoordCylinder(j, &(coord_j[0])) ; CHKERRA(ierr);
    /* Boucle sur les lignes */
    for (i=myProc ; i<nbPtsBEM ; i+=nbProcs) {
      ierr=computeCoordCylinder(i, &(coord_i[0])) ; CHKERRA(ierr);
      {
        ierr=computeKernelBEM(&(coord_i[0]), &(coord_j[0]), i==j, (double _Complex *)&Aij) ;
        if (complexALGO) {
          for (k=0 ; k<nbRHS ; k++) {
#pragma omp atomic
            z_sol[(size_t)k*nbPts+i].r += Aij.r * z_rhs[(size_t)k*nbPts+j].r - Aij.i * z_rhs[(size_t)k*nbPts+j].i ;
#pragma omp atomic
            z_sol[(size_t)k*nbPts+i].i += Aij.r * z_rhs[(size_t)k*nbPts+j].i + Aij.i * z_rhs[(size_t)k*nbPts+j].r ;
          }
        } else {
          for (k=0 ; k<nbRHS ; k++) {
#pragma omp atomic
            d_sol[(size_t)k*nbPts+i] += Aij.r * d_rhs[(size_t)k*nbPts+j] ;
          }
        }
      } /* if testDofNeighbour(.. */
    } /* for (i=0 */

    int jg_bak;
#pragma omp atomic capture
    jg_bak = jg+=sparseRHS ;

#ifdef _OPENMP
    if (omp_get_thread_num()==0)
#endif
    {
      ierr=Mpf_progressBar(myComm, jg_bak+1, nbPtsBEM+1) ;
    }
  } /* for (j=0 */
  ierr=Mpf_progressBar(myComm, nbPtsBEM+1, nbPtsBEM+1) ;

  ierr = sommePara((double*)sol) ; CHKERRQ(ierr) ;

  leave_context();
  return 0 ;
}

/* Allocates an array and stores in it the mat-vec product A.rhs -> sol computed "classicaly" */
int produitClassique(void **sol, MPI_Comm myComm) {
  int ierr;
  double temps_initial, temps_final, temps_cpu;

  void *solCLA = MpfCalloc((size_t)nbPts*nbRHS, complexALGO ? sizeof(Z_type) : sizeof(D_type)) ; CHKPTRQ(solCLA) ;

  if (0) // piece of code to test the relative norms of FEM and BEM parts in the output of the product
    // because if one of the 2 is negligible, then our test_FEMBEM doesn't realy test it...
    if (testedModel==_fembem) {
      double normBEM, normFEM;
      // Compare the norms of the BEM and FEM parts
      ierr = produitClassiqueBEM(2, solCLA, myComm) ; CHKERRQ(ierr) ;
      ierr = computeVecNorm(solCLA, &normBEM); CHKERRQ(ierr);
      memset(solCLA, 0, (size_t)nbPts*nbRHS* (complexALGO ? sizeof(Z_type) : sizeof(D_type)));

      ierr = produitClassiqueFEM(solCLA, myComm) ; CHKERRQ(ierr) ;
      ierr = computeVecNorm(solCLA, &normFEM); CHKERRQ(ierr);
      memset(solCLA, 0, (size_t)nbPts*nbRHS* (complexALGO ? sizeof(Z_type) : sizeof(D_type)));
      Mpf_printf(myComm, "normBEM=%g normFEM=%g ratio FEM/BEM=%g\n", normBEM, normFEM, normFEM/normBEM);
    }
  temps_initial = getTime ();
  ierr = produitClassiqueBEM(2, solCLA, myComm) ; CHKERRQ(ierr) ;
  ierr = produitClassiqueFEM(solCLA, myComm) ; CHKERRQ(ierr) ;
  temps_final = getTime ();
  temps_cpu = temps_final - temps_initial ;
  Mpf_printf(MPI_COMM_WORLD,"<PERFTESTS> TpsCpuClassic = %f \n", temps_cpu) ;
  if (verboseTest) {
    ierr = displayArray("solCLA", solCLA, nbPts, nbRHS) ; CHKERRQ(ierr) ;
  }
  *sol = solCLA;

  return 0;
}
